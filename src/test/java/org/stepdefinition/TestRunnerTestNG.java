package org.stepdefinition;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/Feature/Demo1.feature", glue = "org.stepdefinition",plugin = { "pretty", "html:target/cucumber-testng/cucmberreport.html",
"json:target/cucumber-testng/Cucumber.json"})

public class TestRunnerTestNG extends AbstractTestNGCucumberTests{

}
